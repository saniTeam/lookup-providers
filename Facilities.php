<?php
	include('Conexion/conexion.php');

	
	$name=$_GET["name"];
	//echo $name;
?>
<div class="container">
					
						<div class="row text-center">
							<div class="col-md-10 col-md-offset-1 intro">
							
							<div id="contenedorTotalFacility">
							<div id="contenidosTotalFacility">
							<div id="columna1TotalFacility">
							<div id="contenedorFacility" style="text-align:justify;background:none; border:none;">
								<div id="contenidosFacility">
								
								<div id="columna1Facility"><center><div>General Information</div></center>
								</div>
								</div>
								</div>
							<?php					
							$query = $connection->query("SELECT rp.Title as facility,rp.Phone1 as phone_number,rp.website as site,rp.description as description,rp.lat as lat,rp.lng as lng,rct.Title as island,CONCAT(rp.Opening_Hours,' ',rp.Opening_Hours_To) as openingHour from rawa_providers rp INNER JOIN rawa_cat_territories rct ON rct.Id = rp.TerritoryId where rp.Title like'%%".$name."%%'");
							while($row = $query->fetch_assoc()){ ?>
							<div id="contenedorFacility" style="text-align:justify;">
							<div id="contenidosFacility">
								<div id="columna1Physician1"><center><img id="img1" src="images/PCP/1.jpg"></center></div>
						       </div>
							<div id="contenidosFacility">
								<div id="columna1Facility"><span class="glyphicon glyphicon-globe"></span> <a href="<?php echo $row['site'];?>" target="_blank">Go to Website</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							   </div>
								<div id="contenidosFacility">
								<div id="columna1Facility"><span class="glyphicon glyphicon-time"></span> <?php echo $row['openingHour'];?></div>
							   </div>
							    <div id="contenidosFacility">
								<div id="columna1Facility"><span class="glyphicon glyphicon-earphone"></span> <?php echo $row['phone_number'];?></div>
							   </div>
							   <div id="contenidosFacility">
								<div id="columna1Facility"> <span class="glyphicon glyphicon-pushpin"></span> <?php echo $row['island'];?></div>
							   </div>
							   <div id="contenidosFacility">
								<div id="columna1Facility"><span class="glyphicon glyphicon-map-marker"></span> 
								 <?php $link1='https://maps.google.com?saddr=Current+Location&daddr='; 
										      $lat1 = $row['lat'];
											  $lng1 = $row['lng'];
											  $link = $lat1.','.$lng1;
											  $link2=$link1.$link;    
								 ?>
								<a href="<?php echo $link2 ?>" target="_blank">Get directions</a></div>
							   </div>
							
							
							<?php }  ?>
							</div>
							</div>
							
							<div id="columna2TotalFacility">
							<?php					
							$query = $connection->query("SELECT distinct rp.Title as facility,rp.Phone1 as phone_number,rp.website as site,rp.description as description,rp.lat as lat,rp.lng as lng,rct.Title as island,CONCAT(rp.Opening_Hours,' ',rp.Opening_Hours_To) as openingHour from rawa_providers rp INNER JOIN rawa_cat_territories rct ON rct.Id = rp.TerritoryId where rp.Title like'%%".$name."%%'");


							while($row = $query->fetch_assoc()){ ?>
							<p><?php echo $row['facility'];?> Information.</p>
															<p style="text-align: justify;">
															<?php echo $row['description'];?>
															
															<div id="line1"></div></p>
							<?php $count=2;  }  ?>
							
							
							<?php $query = $connection->query("select CONCAT(am.FirstName,' ',am.LastName) as name, rcs.Title as specialties from rawa_providers rp INNER JOIN rawa_providersmedics rpm ON rp.Id = rpm.ProviderId INNER JOIN aspnet_membership am on am.UserId= rpm.MedicId INNER JOIN rawa_cat_specialities rcs ON rcs.Id=am.SpecialityId where rp.Title like'%%".$name."%%'");
							?><p style="text-align:left;">Physicians</br><?php
								while($row = $query->fetch_assoc()){ ?>
								</br>
								<span class="glyphicon glyphicon-user"></span> <a style="cursor:pointer;text-decoration:underline;" name="<?php echo $row['name'];?>" onclick="javascript:PhyscianNameInformation(this)"><?php echo $row['name'];?></a></br>
								
								<?php echo $row['specialties'];?>
								<?php }  ?>
							</p>
							</div>
							</div>
							</div>
							</div>
