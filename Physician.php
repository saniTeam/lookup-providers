<?php
	include('Conexion/conexion.php');

	
	$physicianName=$_GET["physicianName"];
	//echo $physicianName;

?>
<div class="container">

						<div class="row text-center">
							<div class="col-md-10 col-md-offset-1 intro">
								<div id="contenedorTotalPhysician">
								
								<div id="contenidosTotalPhysician">
								<div id="columna1TotalPhysician">
								<div id="contenedorPhysician" style="text-align:justify;background:none; border:none;">
								<div id="contenidosPhysician">
								
								<div id="columna1Physician"><center><div>Works for</div></center>
								</div>
								</div>
								</div>
								<?php
								$query = $connection->query("Select rp.Title as facility,rp.lat as lat,rp.lng as lng,rp.website as site,CONCAT(am.FirstName,' ',am.LastName) as name,rp.Phone1 as phone_number,am.MedicLicenceNumber as lic_number,rcs.Title as specialty,rp.description as description,rct.Title as island,CONCAT(rp.Opening_Hours,' ',rp.Opening_Hours_To) as openingHour from aspnet_membership am LEFT JOIN rawa_providersmedics rpm ON am.UserId = rpm.MedicId LEFT JOIN rawa_providers rp ON rpm.ProviderId = rp.Id LEFT JOIN rawa_cat_territories rct ON rct.Id = rp.TerritoryId LEFT JOIN rawa_cat_specialities rcs ON am.SpecialityId = rcs.Id where am.FirstName like '%%".$physicianName."%%' or am.LastName like '%%".$physicianName."%%'");
								
								while($row = $query->fetch_assoc()){ ?>
								
								
								
								<div id="contenedorPhysician" style="text-align:justify;">
								
								<div id="contenidosPhysician">
								
								<div id="columna1Physician"><center><a style="cursor:pointer; " name="<?php echo $row['facility'];?>" onclick="javascript:FacilityNameInformation(this)"><?php echo $row['facility'];?></a></center>
								</div>
								</div>
								
								<div id="contenidosPhysician">
								<div id="columna1Physician1"><center><img id="img1" src="images/PCP/1.jpg"></center></div>
						       </div>
								<div id="contenidosPhysician">
								
								<div id="columna1Physician"><span class="glyphicon glyphicon-globe"></span> <a href="<?php echo $row['site'];?>" target="_blank">Go to Website</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
							   </div>
							   
								<div id="contenidosPhysician">
								<div id="columna1Physician"><span class="glyphicon glyphicon-time"></span> <?php echo $row['openingHour'];?></div>
							   </div>
							    <div id="contenidosPhysician">
								<div id="columna1Physician"><span class="glyphicon glyphicon-earphone"></span> <?php echo $row['phone_number'];?></div>
							   </div>
							   <div id="contenidosPhysician">
								<div id="columna1Physician"><span class="glyphicon glyphicon-pushpin"></span> <?php echo $row['island'];?></div>
							   </div>
							   <div id="contenidosPhysician">
								<div id="columna1Physician"><span class="glyphicon glyphicon-map-marker"></span> 
								 <?php $link1='https://maps.google.com?saddr=Current+Location&daddr='; 
										      $lat1 = $row['lat'];
											  $lng1 = $row['lng'];
											  $link = $lat1.','.$lng1;
											  $link2=$link1.$link;    
								 ?>
								<a href="<?php echo $link2 ?>" target="_blank">Get directions</a></div>
							   </div>
							   
						       </div>
							   
							   </br>

							   <?php	}	?>
							
								
								</div>
								<div id="columna2TotalPhysician">
								<?php $query = $connection->query("select CONCAT(am.FirstName,' ',am.LastName)as name,am.MedicLicenceNumber as lic_number,rcs.Title as specialty,am.description as description from aspnet_membership am LEFT JOIN rawa_providersmedics rpm ON am.UserId = rpm.MedicId LEFT JOIN rawa_providers rp ON rpm.ProviderId = rp.Id LEFT JOIN rawa_cat_territories rct ON rct.Id = rp.TerritoryId LEFT JOIN rawa_cat_specialities rcs ON am.SpecialityId = rcs.Id where am.FirstName like '%%".$physicianName."%%' or am.LastName like '%%".$physicianName."%%'");

								$count = 1;
								while($row = $query->fetch_assoc()){ ?>
								
								<?php if($count == 1){ ?>
							<p><?php echo $row['name'];?> Information.</p>
									   <h4><p><span class="glyphicon glyphicon-user"></span> Lic Number: <?php echo $row['lic_number'];?></p></h4>
										<h4><p><span class="glyphicon glyphicon-thumbs-up"></span> Specialty: <?php echo $row['specialty'];?></p></h4>
										<p style="text-align: justify;">
								<?php echo $row['description'];?>
								</p>

								<?php $count=2;  } } ?>
								</div>
							
								
						</div>
						</div>
						</div>
						</div>
						</div>

							

