<?php
include('../Conexion/conexion.php');
// Get parameters from URL

$direction = $_GET["direction"];
// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);



// Search the rows in the markers table
$query = sprintf("SELECT rp.*,rct.Title as island FROM rawa_cat_territories rct INNER JOIN rawa_providers rp on rp.TerritoryId = rct.Id where rct.Title like '%%".$direction."%%'");
$result = mysqli_query($connection,$query);

$result = mysqli_query($connection,$query);


header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @mysqli_fetch_assoc($result)){
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("name", $row['Title']);
  $newnode->setAttribute("island", $row['island']);
  $newnode->setAttribute("lat", $row['lat']);
  $newnode->setAttribute("lng", $row['lng']);
}

echo $dom->saveXML();
?>