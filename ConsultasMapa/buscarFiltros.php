<?php
include('../Conexion/conexion.php');
// Get parameters from URL


$type = $_GET["type"];
$island = $_GET["island"];


if($type != 'nulo' && $island != 'nulo'){

// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);


$query = sprintf("SELECT rp.*,rct.Title as island FROM rawa_providers rp LEFT JOIN rawa_cat_territories rct ON rp.TerritoryId = rct.Id LEFT JOIN rawa_cat_institutionalsectors rci on rp.InstitutionalSectorId=rci.Id LEFT JOIN rawa_cat_providerTypes rcpt on rp.ProviderTypeId=rcpt.Id LEFT JOIN rawa_providersmedics rpm on rp.Id=rpm.ProviderId LEFT JOIN aspnet_membership am on am.UserId=rpm.MedicId LEFT JOIN rawa_cat_specialities rcs ON am.SpecialityId=rcs.Id where rcpt.Id='".$type."' and rct.Id='".$island."'");
$result = mysqli_query($connection,$query);

header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @mysqli_fetch_assoc($result)){
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("name", $row['Title']);
  $newnode->setAttribute("island", $row['island']);
  $newnode->setAttribute("lat", $row['lat']);
  $newnode->setAttribute("lng", $row['lng']);
}
echo $dom->saveXML();

}else if($type != 'nulo' && $island == 'nulo' ){
	// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

$query = sprintf("SELECT rp.*,rct.Title as island FROM rawa_providers rp LEFT JOIN rawa_cat_territories rct on rp.TerritoryId = rct.Id LEFT JOIN rawa_cat_institutionalsectors rci on rp.InstitutionalSectorId=rci.Id LEFT JOIN rawa_cat_providerTypes rcpt on rp.ProviderTypeId=rcpt.Id LEFT JOIN rawa_providersmedics rpm on rp.Id=rpm.ProviderId LEFT JOIN aspnet_membership am on am.UserId=rpm.MedicId LEFT JOIN rawa_cat_specialities rcs ON am.SpecialityId=rcs.Id where rcpt.Id='".$type."'");
$result = mysqli_query($connection,$query);
header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @mysqli_fetch_assoc($result)){
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("name", $row['Title']);
  $newnode->setAttribute("island", $row['island']);
  $newnode->setAttribute("lat", $row['lat']);
  $newnode->setAttribute("lng", $row['lng']);
}
echo $dom->saveXML();

}else if($type == 'nulo' && $island != 'nulo'){
	$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

$query = sprintf("SELECT rp.*,rct.Title as island FROM rawa_providers rp LEFT JOIN rawa_cat_territories rct on rp.TerritoryId = rct.Id LEFT JOIN rawa_cat_institutionalsectors rci on rp.InstitutionalSectorId=rci.Id LEFT JOIN rawa_cat_providerTypes rcpt on rp.ProviderTypeId=rcpt.Id LEFT JOIN rawa_providersmedics rpm on rp.Id=rpm.ProviderId LEFT JOIN aspnet_membership am on am.UserId=rpm.MedicId LEFT JOIN rawa_cat_specialities rcs ON am.SpecialityId=rcs.Id where rct.Id='".$island."'");
$result = mysqli_query($connection,$query);
header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @mysqli_fetch_assoc($result)){
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("name", $row['Title']);
  $newnode->setAttribute("island", $row['island']);
  $newnode->setAttribute("lat", $row['lat']);
  $newnode->setAttribute("lng", $row['lng']);
}
echo $dom->saveXML();
}
?>