<?php
include('../Conexion/conexion.php');
// Get parameters from URL

$name = $_GET["name"];
//echo $name;
$radius = 5;
// Start XML file, create parent node
$dom = new DOMDocument("1.0");
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

$query1 = sprintf("select rp.*,rct.Title as island from rawa_providers rp INNER JOIN rawa_cat_territories rct ON rct.Id = rp.TerritoryId where rp.Title like '%%".$name."%%'");
$result1 = mysqli_query($connection,$query1);
while($row = @mysqli_fetch_assoc($result1)){ 
$_SESSION["lat"]= $row['lat'];
$_SESSION["lng"]= $row['lng'];

}
$center_lat=$_SESSION["lat"];
$center_lng=$_SESSION["lng"];


$query = sprintf("select rp.*, ( 3959 * acos( cos( radians('%s') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians('%s') ) + sin( radians('%s') ) * sin( radians( lat ) ) ) ) AS distance from rawa_providers rp HAVING distance < '".$radius."'",
  mysqli_real_escape_string($connection,$center_lat),
  mysqli_real_escape_string($connection,$center_lng),
  mysqli_real_escape_string($connection,$center_lat),
  mysqli_real_escape_string($connection,$radius));

$result = mysqli_query($connection,$query);
header("Content-type: text/xml");

// Iterate through the rows, adding XML nodes for each
while ($row = @mysqli_fetch_assoc($result)){
  
  $node = $dom->createElement("marker");
  $newnode = $parnode->appendChild($node);
  $newnode->setAttribute("name", $row['Title']);
  //$newnode->setAttribute("island", $row['island']);
  $newnode->setAttribute("lat", $row['lat']);
  $newnode->setAttribute("lng", $row['lng']);
  $newnode->setAttribute("distance", $row['distance']);
}

echo $dom->saveXML();
?>