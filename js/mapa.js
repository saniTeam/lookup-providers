
    var map;
    var markers = [];
    var infoWindow;
    var locationSelect;


	function load(){

      map = new google.maps.Map(document.getElementById("map"), {
        center: new google.maps.LatLng(24.520967,-77.1645969),
	scrollwheel: false,
        zoom: 5,
        mapTypeId: 'roadmap',
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
      });
      infoWindow = new google.maps.InfoWindow();
	  var center = new google.maps.LatLng(24.520967,-77.1645969);
      marcaCuandoEntra(center);
      locationSelect = document.getElementById("locationSelect");
      locationSelect.onchange = function() {
        var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
        if (markerNum != "none"){
          google.maps.event.trigger(markers[markerNum], 'click');
        }
      };
   }  
  

    function searchLocations(){
	clearLocations();
	document.getElementById('FacilityName').value = 0;
    document.getElementById('FacilityType').disabled = false;
    document.getElementById('IslandType').disabled = false;
	document.getElementById('searchFiltros').disabled = false;	
	document.getElementById('information').style.display= 'none';
    searchLocationsNear();   
   }  

    function habilitarTodo(){


clearLocations();
document.getElementById('FacilityName').value = 0; 
document.getElementById('FacilityType').value = 0;
document.getElementById('IslandType').value = 0;
//document.getElementById('SpecialtyPhysician').value = 1;
document.getElementById('FacilityType').disabled = false;
//document.getElementById('SpecialtyPhysician').disabled = false;
document.getElementById('IslandType').disabled = false;
document.getElementById('addressInput').disabled = false;
document.getElementById('addressInput').value = '';
document.getElementById('searchText').disabled = false;
document.getElementById('searchFiltros').disabled = false;
document.getElementById('information').style.display = 'none';
var center = new google.maps.LatLng(24.3777265,-80.4742536);
marcaCuandoEntra(center);
 window.location.href="#home";
       e.preventDefault();
	   $("html, body").stop().animate({ scrollTop: 0 }, "slow");
	   
   } 
function buscarFacilityDesdeTabla(valor){

clearLocations();
//alert(valor);
     var searchUrl = 'ConsultasMapa/buscarFacilityDesdeTabla.php?name=' + valor;
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
     //alert(markerNodes.length);
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         //var island = markerNodes[i].getAttribute("island");
         var distance = 100;
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));
         var island='Bahamas';
         createOption(name, distance, i);
         createMarkerDesdeTabla(latlng, name, island);
        
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   }else{
		   var center = new google.maps.LatLng(24.3777265,-80.4742536);
           marcaCuandoEntra(center);
		   //alert('No se encontro facility');
		   swal("1 Alert!", "Your search criteria returns no results");
	   }
      });
	}
	
    function buscarFacility(){
	var facilityName = document.getElementById("FacilityName").value;
	var otra = document.getElementById("valorInput").value = facilityName;
	var final1 = otra.split(" ");
	var listo = final1[1];
    document.getElementById('FacilityType').disabled = true;
    document.getElementById('IslandType').disabled = true;
	document.getElementById('searchFiltros').disabled = true;
clearLocations();
      String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ""); };
    
	 var facilityId = document.getElementById("FacilityName").value.trim();
     var searchUrl = 'ConsultasMapa/buscarFacility.php?id=' + facilityId;
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
     
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var distance = markerNodes[i].getAttribute("distance");
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));
		 island='Bahamas';
         createOption(name, distance, island);
         createMarker(latlng, name, island);
        
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   }else{
		   var center = new google.maps.LatLng(24.3777265,-80.4742536);
           marcaCuandoEntra(center);
		   //alert('No se encontro facility');
		   swal("2 Alert!", "Your search criteria returns no results");
	   }
	   cargaFacility(listo);
      });
	}

	function cargaFacility(id){	    
		$('#posts_content').load('ConsultasTabla/cargarDoctoresFacility.php?valor=' + id + '&borrar=si');
		//document.getElementById("cuadroPorDireccion").style.display = 'block';
	}
function buscarNombreDesdeTabla(valor){
clearLocations();

    
	
     var searchUrl = 'ConsultasMapa/buscarNombre.php?name=' + valor;
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       //alert(markerNodes.length);
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var distance = 100;
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));
         
         createOption(name, distance, i);
         createMarkerDesdeTabla(latlng, name, island);    
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   }else{
		   document.getElementById('information').style.display = 'none';
		   var center = new google.maps.LatLng(24.3777265,-80.4742536);
		   
           marcaCuandoEntra(center);
		   //alert('No se encontro');
		  swal("3 Alert!", "Your search criteria returns no results");
	   }
	   cargarDoctoresBuscadorNombre(valor);
      });
}
	function buscarNombreDoctor(){
clearLocations();
var physicianName = document.getElementById("addressInput").value;

     var searchUrl = 'ConsultasMapa/buscarNombre.php?name=' + physicianName;
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var distance = 100;
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));

         createOption(name, distance, i);
         createMarker(latlng, name, island);    
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   }else{
		   var center = new google.maps.LatLng(24.3777265,-80.4742536);
           marcaCuandoEntra(center);
		   //alert('No se encontro');
		  swal("4 Alert!", "Your search criteria returns no results");
	   }
	   cargarDoctoresBuscadorNombre(physicianName);
      });
	
}

	function cargarDoctoresBuscadorNombre(direction){
		//var direction = document.getElementById("addressInput").value.trim();
		$('#posts_content').load('ConsultasTabla/cargarDoctoresNombre.php?valor=' + direction + '&borrar=si');
		//document.getElementById("cuadroPorDireccion").style.display = 'block';
	} 

	function encontrarPorFiltros(){
clearLocations();
document.getElementById('information').style.display= 'none';
String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ""); };

var type = document.getElementById('FacilityType').value.trim(); //posicion
var  islandId = document.getElementById('IslandType').value.trim();
	
if(type=='0'){
	type = 'nulo';
}
if(islandId=='0'){
	islandId = 'nulo';
}
//alert(islandId);
     var searchUrl = 'ConsultasMapa/buscarFiltros.php?type=' + type + '&island=' + islandId ;
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var distance = 100;
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));

         createOption(name, distance, i);
         createMarker(latlng, name, island);     
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   }else{
		   var center = new google.maps.LatLng(24.3777265,-80.4742536);
           marcaCuandoEntra(center);
		   swal("5 Alert!", "Your search criteria returns no results");
		   //alert('No se encontro facility');
	   }
	   cargarDoctoresBuscadorFiltros(type,islandId);
      });
   } 

    function cargarDoctoresBuscadorFiltros(type,islandId){
//alert(islandId);
		$('#posts_content').load('ConsultasTabla/cargarDoctoresFiltros.php?type=' + type + '&island=' + islandId + '&borrar=si');
	}   

	function clearLocations() {
     infoWindow.close();
     for (var i = 0; i < markers.length; i++) {
       markers[i].setMap(null);
     }
     markers.length = 0;

     locationSelect.innerHTML = "";
     var option = document.createElement("option");
     option.value = "none";
     option.innerHTML = "See all results:";
     locationSelect.appendChild(option);
   }   

    function marcaCuandoEntra(center) {
     var searchUrl = 'ConsultasMapa/cargaInicial.php?lat=' + center.lat() + '&lng=' + center.lng() + '&radius=6000';
     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var distance = parseFloat(markerNodes[i].getAttribute("distance"));
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));

         createOption(name, distance, i);
         createMarker(latlng, name, island);        
       }
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   cargarPuntosInicial();
      });
    }
function cargarPuntosInicial(){
	
		//alert('llegue');
		$('#posts_content').load('ConsultasTabla/cargarDoctoresInicial.php');
		document.getElementById("cuadroPorDireccion").style.display = 'block';
	}
	
function searchLocationsNear() {
     clearLocations();
	 
var final2 = document.getElementById('addressInput').value;
var listo1 = final2.split(" ");
var direction = listo1[0];
//alert(direction);
if(direction==''){
	 var center = new google.maps.LatLng(24.3777265,-80.4742536);
           marcaCuandoEntra(center);
	swal("6 Alert!", "Please insert a correct criteria");
}else{
     var searchUrl = 'ConsultasMapa/buscarDireccion.php?direction=' + direction;

     downloadUrl(searchUrl, function(data) {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       //var bounds = new google.maps.LatLngBounds();
	    //alert(markerNodes.length);
	   if(markerNodes.length != 0){
       for (var i = 0; i < markerNodes.length; i++) {
         var name = markerNodes[i].getAttribute("name");
         var island = markerNodes[i].getAttribute("island");
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));
	     var distance = parseFloat(markerNodes[i].getAttribute("distance"));
         createOption(name, distance, i);
         createMarker(latlng, name, island);
         //bounds.extend(latlng);
       }
       //map.fitBounds(bounds);
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
	   cargarDoctoresBuscadorDireccion(direction);
	   }else{
		   buscarNombreDoctor();
	   }
      });
	   }
    }

	function cargarDoctoresBuscadorDireccion(direction){
	
		$('#posts_content').load('ConsultasTabla/cargarDoctoresDireccion.php?valor=' + direction + '&borrar=si');
	}
	
	function cargaFacilityDesdeMapa(name){
		//alert(name);

var listo1 = name.split(" ");
var name = listo1[0];
//alert(name);
		$('#posts_content').load('ConsultasTabla/cargarDoctoresDesdeMarket.php?valor=' + name + '&borrar=si');
		//document.getElementById("cuadroPorDireccion").style.display = 'block';
	}
function createMarkerDesdeTabla(latlng, name, island) {
      
	  //alert(latlng);
	  //alert(island);
	  //alert(name);
      var marker = new google.maps.Marker({
        map: map,
		position: latlng
      });
      map.setZoom(12);
      map.panTo(marker.position);
      google.maps.event.addListener(marker, 'click', function() {
	 document.getElementById('valorInput').value = name;
	  FacilityNameInformationClickPuntoMapa();
	  map.setZoom(15);
      map.panTo(marker.position);
	  var link1='https://maps.google.com?saddr=Current+Location&daddr=';
	  //var link2= latlng.substring(1,latlng.length);
	  var link=link1.concat(latlng);
	  //alert(link);
	  var html = '<b>' + name + '</b> <br/>' + island + '</br> <a href="'+link+'" target="_blank">Get Directions</a>';;
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      markers.push(marker);
    }
	function createMarker(latlng, name, island) {
      //alert(latlng);
      var marker = new google.maps.Marker({
        map: map,
		position: latlng
      });
      map.setZoom(6);
      map.panTo(marker.position);
      google.maps.event.addListener(marker, 'click', function() {
	  cargaFacilityDesdeMapa(name);
	  document.getElementById('valorInput').value = name;
	  FacilityNameInformationClickPuntoMapa();
	  map.setZoom(12);
      map.panTo(marker.position);
	  var link1='https://maps.google.com?saddr=Current+Location&daddr=';
	  //var link2= latlng.substring(1,latlng.length);
	  var link=link1.concat(latlng);
	  //alert(link);
	  var html = '<b>' + name + '</b> <br/>' + island + '</br> <a href="'+link+'" target="_blank">Get Directions</a>';
	  
	    infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
      markers.push(marker);
    }

	function createOption(name, distance, num) {
      var option = document.createElement("option");
      option.value = num;
      option.innerHTML = name;
      locationSelect.appendChild(option);
    }

	function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request.responseText, request.status);
        }
      };
      request.open('GET', url, true);
      request.send(null);
    }

	function parseXml(str) {
      if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.loadXML(str);
        return doc;
      } else if (window.DOMParser) {
        return (new DOMParser).parseFromString(str, 'text/xml');
      }
    }
	function FacilityNameInformationClickPuntoMapa() {
var final2 = document.getElementById('valorInput').value;
//alert(final2);
var listo1 = final2.split(" ");
var name = listo1[0];

$('#facilityInformation').load('Facilities.php?name=' + name);
document.getElementById('information').style.display = 'block';
    }
	
function recargarPagina(){
		location.reload(true);
	}

	function FacilityNameInformation(valor) {

var final1=valor.innerHTML;
var listo = final1.split(" ");
var name = listo[0];
buscarFacilityDesdeTabla(name);

$('#facilityInformation').load('Facilities.php?name=' + name);
document.getElementById('information').style.display = 'block';
    }
	function PhyscianNameInformation(valor) {

var final1=valor.innerHTML;
var listo = final1.split(" ");
var physicianName = listo[0];
//alert(physicianName);
buscarNombreDesdeTabla(physicianName);

$('#facilityInformation').load('Physician.php?physicianName=' + physicianName);
document.getElementById('information').style.display = 'block';
	}
	
$(document).ready(function(){
$('#map1').load('map.php#ShowMap');
$("body").append("<div class='scrollup'><img src='js/img/NHI/top.png'></div>");
$(window).scroll(function(){
if ($(this).scrollTop() > 120) $('.scrollup').fadeIn();
else $('.scrollup').fadeOut();
});
$(document).on("click",".scrollup",function(e){
e.preventDefault();
$("html, body").stop().animate({ scrollTop: 0 }, "slow");
});
});
$(window).scroll(function() {
if ($(this).scrollTop()>0)
     {
		 $('#message').fadeOut();
	 }
	  else
     {
		 $('#message').fadeIn();
	 }
    if ($(this).scrollTop()>400)
     { 
		$('#home').fadeOut();
     }
    else
     {
       $('#home').fadeIn(); 
     }
 });

	function doNothing() {}
	
	// Show loading overlay when ajax request starts
$( document ).ajaxStart(function() {
    $('.loading-overlay').show();
});
// Hide loading overlay when ajax request completes
$( document ).ajaxStop(function() {
    $('.loading-overlay').hide();
});