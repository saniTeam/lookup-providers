<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

    <title>Search Facility Information</title>

	<noscript>
		<link href="js/css/noscript.css" rel="stylesheet" />
	</noscript>
    <!-- Bootstrap core CSS -->

  <link href="js/css/bootstrap.min.css" rel="stylesheet">
  <link href="js/css/style.css" rel="stylesheet">
  <link href="js/css/overwrite.css" rel="stylesheet">
  <link rel="stylesheet" href="js/sweetalert.css">
  <link rel="stylesheet" href="js/sweetalertMensajeInicial.css">

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="icon" href="js/ico/logo.png">
  
  
  
<script type="text/javascript" src="js/js/modernizr.custom.86080.js"></script>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script> 
<script src="js/sweetalert-dev.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBwaHyIsDZrPQVS2OTSks-s4sAsnvLu8tM&language=en" type="text/javascript"></script>      
<script src="js/jquery.min.js"></script>
<script src="js/mapa.js"></script>


</head>
<style>
/* full px */
#line1{
border-top: 3px solid #ebeef1;
 margin: 0px 310px 0px -40%;
}
.sweet-alertMensajeInicial{
	display: block;
margin-top: -15%;
margin-left: 12%;

}
.navbar, .navbar-fixed-bottom, .navbar-fixed-top {
    position: fixed;
    right: 0;
    left: 0;
    z-index: 1030;
	margin: 0px;
}
.scrollup {
  //background: red;
  bottom: 1.5em;
  color: white;
  cursor: pointer;
  display: none;
  padding: .25em .5em;
  position: fixed;
  right: 1.5em;
}

.btndefaultClear{
	margin: -15px 0px 5px 90%;
	background: rgb(72, 166, 72);
	height:42px;
	color: white;
	border:solid;
	border-color:red;
}
.home-list {
    float: left;
    width: 100%;
    margin: -213% 0px 0% 10%;
    padding: 0;
}
.homelistPhysician{
	float: left;
    width: 100%;
    margin: -404% 0px 0px -160%;
    padding: 0;
}

.homelistPhysician li{
	list-style:none;
	float:left;
	width:100%;
	margin:0;
	padding:0;
	font-size:14px;
	font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
	line-height:18px;
	position:relative;
	margin-bottom:10px;
}

.homelistPhysician li h4{
	margin-bottom:15px;
}

.col-md-offset-1 {
    margin-left: 25.333%;
}

a {
    color: #00A8D0;
    text-decoration: none;
}
table {
  border-collapse: collapse;
  width: 100%;
}
th, td {
  padding: 0.25rem;
  border: 1px solid #white;
  width: 100px;
}
tbody tr:nth-child(odd) {
  background: white;
}
.centro{
  padding: 0.5rem;
  background: #00A8D0 ;
  color: white;
  text-align: left;
  font-size: 21px;

}

#cuadroPorDireccion{
	width: 115%;
	//background: #F8F8F8 ;
	margin: 15px auto;

}
#titulo{
	width: 100%;
	background: #00A8D0;
	color:white;

}

#TotalResults{
margin: 0px 0px 0px -10%;
}
/*Facilities Contenedor*/

#contenedorTotalFacility {
display: table;
width: 1175px;
margin: 0% 7%;
}
#contenidosTotalFacility {
    display: table-row;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
    display: table-cell;
    vertical-align: top;

}

#contenedorFacility {
display: table;
    width: 300px;
    margin: 0% -150%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosFacility {
    display: table-row;
}
#columna1Facility, #columna2Facility, #columna3Facility {
    display: table-cell;
    padding: 0px 10%;
}



/*Physician Contenedor*/

#contenedorTotalPhysician {
display: table;
width: 1175px;
margin: 0% 7%;
}
#contenidosTotalPhysician {
    display: table-row;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
    display: table-cell;
    vertical-align: top;

}

#contenedorPhysician {
display: table;
    width: 300px;
    margin: 0% -150%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosPhysician {
    display: table-row;
}
#columna1Physician, #columna2Physician, #columna3Physician {
    display: table-cell;
    padding: 0px 10%;
}

#img1{
	width:50%;
	heigth:20%;
}

p {
   margin: 0px 310px 0px -40%;
}

#contenedor {
display: table;
width: 300px;
text-align: left;
margin: -10px -115px auto;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    vertical-align: middle;
    padding: 10px;
}

#contenidos #columnaFilter1,#columnaFilter2{
	display: table-cell;
    vertical-align: middle;
	padding: 10px 5px 0px 2px;
}

#columnaFilter1 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 35px;
}

#columnaFilter2 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 35px;
}

.navbar-inverse .navbar-nav > li > a {     
	color: white;	
}
.navbar-collapse ul li {
     padding-top: 0px;
     padding-bottom: 0px;
}
.navbar-collapse ul li a {
    padding-top: 0px;
    padding-bottom: 0px;
}
.navbar-inverse {
    background-color: white;
}
.navbar-form {
    width: 100%;
    padding-top: 15px;
    padding-bottom: 20px;
    margin-right: 20px;
    margin-left: 200px;
    border: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
	max-width: 100%;

}

.navbar-brand img {
    width: 250px;
    height: 85px;
	margin: -15px 0 0 -130px;
}
.btn btn-default{
	margin: 0px 0px 0px -2px;
	background: rgb(72, 166, 72);
	height:42px;
	border:solid;
	border-color:#48A648;
}
body {overflow-x:hidden;}
/* 1280 px */
@media (max-width: 1280px){
	#line1{
border-top: 3px solid #ebeef1;
 margin: 0px 360px 0px -20%;
}
body {overflow-x:hidden;}

#contenedorTotalPhysician {
    display: table;
    width: 1115px;
    margin: 0% 6%
}
#contenidosTotalPhysician {
    display: table-row;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
    display: table-cell;
    vertical-align: top;

}

#contenedorPhysician {
display: table;
    width: 154%;
    text-align: left;
    margin: 1% -170%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosPhysician {
    display: table-row;
}
#columna1Physician, #columna2Physician, #columna3Physician {
    display: table-cell;
    padding: 0px 10%;
}

#contenedorTotalFacility {
display: table;
    width: 1155px;
    margin: 0% 2%;
}
#contenidosTotalFacility {
    display: table-row;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
    display: table-cell;
    vertical-align: top;

}

#contenedorFacility {
display: table;
    width: 300px;
    margin: 0% -95%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosFacility {
    display: table-row;
}
#columna1Facility, #columna2Facility, #columna3Facility {
    display: table-cell;
    padding: 0px 10%;
}

#img1{
	width:50%;
	heigth:20%;
}

p {
   margin: 0px 360px 0px -20%;
}

	
	.homelistPhysician {
    float: left;
    width: 100%;
    margin: -433% 0px 0px -130%;
    padding: 0;
}
.home-list {
    float: left;
    width: 100%;
    margin: -213% 0px 0% 10%;
    padding: 0;
}

.sweet-alertMensajeInicial {
    display: block;
    margin-top: -15%;
    margin-left: 8%;
}

.btndefaultClear{
	margin: -15px 0px 5px 90%;
	background: rgb(72, 166, 72);
	height:42px;
	color: white;
	border:solid;
	border-color:red;
}

table {
  border-collapse: collapse;
  width: 100%; 
  margin-left:-90;
}
#titulo{
	width: 100%;
	background: #00A8D0;
	color:white;
	margin-left:-90;

}
	.navbar-brand img {
    width: 250px;
    height: 85px;
	margin: -15px 0 0 -15px;
}
#contenedor {
    display: table;
    /* width: 125%; */
    text-align: left;
    margin: -10px -100%;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    vertical-align: middle;
    padding: 10px;
}

#contenidos #columnaFilter1,#columnaFilter2{
	display: table-cell;
    vertical-align: middle;
}

#columnaFilter1 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}

#columnaFilter2 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}
}
/* 1024 px */
@media (max-width: 1024px){
#line1{
border-top: 3px solid #ebeef1;
 margin: 0px 15% 0px -55%;
}
#contenedorTotalPhysician {
display: table;
    width: 700px;
    margin: 0% 5%;
}
#contenidosTotalPhysician {
    display: table-row;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
    display: table-cell;
    vertical-align: top;

}
body {overflow-x:hidden;}
#contenedorPhysician {
   display: table;
    width: 282px;
    margin: 0% -90%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosPhysician {
    display: table-row;
}
#columna1Physician, #columna2Physician, #columna3Physician {
    display: table-cell;
    padding: 0px 10%;
}

#contenedorTotalFacility {
display: table;
width: 715px;
margin: 0% 7%;
}
#contenidosTotalFacility {
    display: table-row;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
    display: table-cell;
    vertical-align: top;

}

#contenedorFacility {
display: table;
    width: 300px;
    margin: 0% -95%;
    border-style: solid;
	border-color: #DCEBF9;
	background: #DCEBF9;
}
#contenidosFacility {
    display: table-row;
}
#columna1Facility, #columna2Facility, #columna3Facility {
    display: table-cell;
    padding: 0px 10%;
}

#img1{
	width:50%;
	heigth:20%;
}

p {
margin: 0px 15% 0px -55%;
}

#cuadroPorDireccion {
width: 100%;
    margin: 15px 8%;
}
	
	        .navbar-inverse {
            background-color: white;
			}
.navbar-form {
    width: 100%;
    padding-top: 12px;
    padding-bottom: -1px;
    margin-right: 0px;
    margin-left: 20%;
    border: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
    max-width: 100%;
}
	.btndefaultClear{
	margin: -15px 0px 5px 80%;
	background: rgb(72, 166, 72);
	height:42px;
	color: white;
	border:solid;
	border-color:red;
}
#contenedor {
    display: table;
    width: 50%;
    text-align: left;
    margin: -10px -40%
}


.home-list {
    float: left;
    width: 100%;
    margin: 0% 0px -95% 208%;
    padding: 0;
}
.homelistPhysician {
    float: left;
    width: 100%;
    margin: 0 0px 0px 110%;
    padding: 0;
}
.sweet-alertMensajeInicial {
    display: block;
    margin-top: -23%;
    margin-left: 0%;
}
table {
  border-collapse: collapse;
  width: 100%; 
  margin-left:-70;
}
#titulo{
	width: 100%;
	background: #00A8D0;
	color:white;
	margin-left:-70;

}
#TotalResults {
    margin: 0px 0px 0px -14%;
}
}


/* 800 px */
@media (max-width: 800px){
#line1{
border-top: 3px solid #ebeef1;
       margin: 5px 10% 0px -38%;
}	
#contenedorPhysician {
display: table;
    width: 100%;
    text-align: left;
    margin: 0% -22%;
}
#contenedorTotalPhysician {
display: table;
    width: 80%;
    margin: 0% 11%;
}
#contenidosTotalPhysician {
    display: inline-block;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
   display: table-footer-group;

}
#contenedorTotalFacility {
    display: table;
    width: 100%;
    margin: 0% 4%;
}
#contenidosTotalFacility {
    display: inline-block;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
   display: table-footer-group;

}
#contenedorFacility {
    display: table;
    width: 285px;
    margin: 5% -4%;
    border-style: solid;
    border-color: #DCEBF9;
    background: #DCEBF9;
}

#img1{
	width:50%;
	heigth:20%;
}
p {
       margin: 5px 10% 0px -38%;
}
#cuadroPorDireccion {
    width: 96%;
    margin: 0px 47px;
}
	
	.homelistPhysician {
    float: left;
    width: 100%;
    margin: -404% 0px 0px -160%;
    padding: 0;
}

.sweet-alertMensajeInicial {
    display: block;
    margin-top: -40%;
    margin-left: 22%;
    margin-right: 0%;
    padding: 0% 36% 0% 0%;
}
	.btndefaultClear{
	margin: -15px 0px 5px 80%;
	background: rgb(72, 166, 72);
	height:42px;
	color: white;
	border:solid;
	border-color:red;
}
/* Datagrid */
a {
    color: #00A8D0;
    text-decoration: none;
}
table {
  border-collapse: collapse;
  width: 100%; 
}
th, td {
  padding: 0.25rem;
  border: 1px solid #ccc;
}
tbody tr:nth-child(odd) {
  //background: #eee;
}
.centro{
  padding: 0.5rem;
  background: #00A8D0 ;
  color: white;
  text-align: center;
  font-size: 21px;

}

#titulo{
	width: 110%;
	background: #00A8D0;
	color:white;

}
	#contenedor {
    display: table;
    width: 50%;
    text-align: left;
    margin: 0px 20%;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    vertical-align: middle;
    padding: 10px;
}

#contenidos #columnaFilter1,#columnaFilter2{
	display: table-cell;
    vertical-align: middle;
}

#columnaFilter1 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}

#columnaFilter2 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}

        .navbar-inverse .navbar-nav > li > a {     
			color: white;	
        }
        .navbar-collapse ul li {
            padding-top: 0px;
            padding-bottom: 0px;
        }
          .navbar-collapse ul li a {
                padding-top: 0px;
                padding-bottom: 0px;
            }
        .navbar-inverse {
            background-color: white;
        }
.navbar-form {
    width: 85%;
    padding-top: 15px;
    padding-bottom: 11px;
    margin-right: 0px;
    margin-left: 14%;
    border: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
}

.navbar-brand img {
    width: 250px;
    height: 85px;
	margin: -15px 0 0 -15px;
}
.btn btn-default{
	margin: 0px 0px 0px -2px;
}
#TotalResults {
    margin: 0px 0px 0px -16%;
}
}
@media (max-width: 650px){
	.sweet-alertMensajeInicial {
    display: block;
    margin-top: -40%;
    margin-left: 8%;
    margin-right: 0%;
    padding: 0% 36% 0% 0%;
}
}

/* 500 px */
@media (max-width: 500px){
#line1{	
border-top: 3px solid #ebeef1;
       margin: 5px 10% 0px -38%;
}
.btndefaultClear {
    margin: -20px 0px 5px 65%;
    background: rgb(72, 166, 72);
    height: 42px;
    color: white;
    border: solid;
    border-color: red;
}
.sweet-alertMensajeInicial {
    display: block;
    margin-top: -50%;
    margin-left: 61%;
    margin-right: 2%;
    padding: 1%;
}
.navbar-form {
    width: 100%;
    padding-top: 15px;
    padding-bottom: 20px;
    margin-right: 20px;
    margin-left: 10%;
    border: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
}
#img1{
	width:50%;
	heigth:20%;
}
#contenedor {
    display: table;
    width: 300px;
    text-align: left;
    margin: 0px 4%;
}
#contenidos {
    display: table-row;
}
#columna1, #columna2, #columna3 {
    display: table-cell;
    vertical-align: middle;
    padding: 10px;
}

#contenidos #columnaFilter1,#columnaFilter2{
	display: table-cell;
    vertical-align: middle;
}

#columnaFilter1 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}

#columnaFilter2 h3{
	font-size:30px; 
	width: 130%;
	text-align:left;
	background: #00A8D0;
	color:white;
	height: 34px;
}

	#contenedorPhysician {
display: table;
    width: 100%;
    text-align: left;
    margin: 0% -22%;
}
#contenedorTotalPhysician {
    display: table;
    width: 100%;
    margin: 0% 4%;
}
#contenidosTotalPhysician {
    display: inline-block;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
   display: table-footer-group;

}

#contenedorTotalFacility {
    display: table;
    width: 100%;
    margin: 0% 4%;
}
#contenidosTotalFacility {
    display: inline-block;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
   display: table-footer-group;

}
#contenedorFacility {
    display: table;
    width: 285px;
    margin: 5% -16%;
    border-style: solid;
    border-color: #DCEBF9;
    background: #DCEBF9;
}
p {
       margin: 5px 10% 0px -38%;
}

        .navbar-inverse .navbar-nav > li > a {     
			color: white;	
        }
        .navbar-collapse ul li {
            padding-top: 0px;
            padding-bottom: 0px;
        }
          .navbar-collapse ul li a {
                padding-top: 0px;
                padding-bottom: 0px;
            }
        .navbar-inverse {
            background-color: white;
        }
.navbar-brand img {
    width: 45%;
    height: 50px;
    margin: 0% 0 0 -8%;
}
.btn btn-default{
	margin: 0px 0px 0px -2px;
}
#cuadroPorDireccion {
width: 96%;
    margin: 0px 45px;
}
#TotalResults {
    margin: 0px 0px 0px -25%;
}
}

@media (max-width: 450px){
#line1{
	border-top: 3px solid #ebeef1;
	margin: 5px 10% 0px -38%;
}
#img1{
	width:50%;
	heigth:20%;
}
#contenedor {
display: table;
    width: 285px;
    text-align: left;
    margin: 0px -7%;
}

.sweet-alertMensajeInicial {
    display: block;
	margin-top: -26%;
    margin-left: 59%;
    margin-right: -2%;
    padding: 1%;
}

	#contenedorPhysician {
    display: table;
    width: 125%;
    text-align: left;
    margin: 0% -35%;
}
#contenedorTotalPhysician {
    display: table;
    width: 100%;
    margin: 0% 4%;
}
#contenidosTotalPhysician {
    display: inline-block;
}
#columna1TotalPhysician, #columna2TotalPhysician, #columna3TotalPhysician {
   display: table-footer-group;

}


#contenedorTotalFacility {
    display: table;
    width: 100%;
    margin: 0% 4%;
}
#contenidosTotalFacility {
    display: inline-block;
}
#columna1TotalFacility, #columna2TotalFacility, #columna3TotalFacility {
   display: table-footer-group;

}
#contenedorFacility {
    display: table;
    width: 285px;
    margin: 5% -27%;
    border-style: solid;
    border-color: #DCEBF9;
    background: #DCEBF9;
}

p {
      margin: 5px 20% 0px -35%;
}
#cuadroPorDireccion {
    width: 99%;
    margin: 0px 33px;
}
#TotalResults {
    margin: 0px 0px 0px -30%;
}
}

</style>