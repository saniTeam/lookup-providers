<?php
$username="root";
$password="123456";
$database="dbo";
$localhost="localhost";

// Opens a connection to a mySQL server
$connection=mysqli_connect ($localhost, $username, $password);
if (!$connection) {
  die("Not connected : " . mysqli_error());
}

// Set the active mySQL database
$db_selected = mysqli_select_db($connection, $database);
if (!$db_selected) {
  die ("Can\'t use db : " . mysqli_error());
}